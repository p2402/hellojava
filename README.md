# HelloJava

## Descrizione
Prima esercitazione per prendere confidenza con java

## Traccia

Definisci una classe Parallelogramma con i seguenti attributi: Lato uno e Lato 2.
Deve essere possibile calcolare:
1. Diagonali
2. L'area
3. Perimetro del parallelogramma
4. Tipologia di parallelogramma (quadrato, rombo, etc) 

**IMPORTANTE** non dimenticare l' uso dei **package**  e **SRP** (single responsibility principle)